# README #
## Usage instruction

1. Go to the folder where contains all the .yaml files that to be merged in the terminal 

2. Run script  

   ```
   find $(pwd) -type f -path "*configs*/*" -name "*.yaml" > ../merge_configs/hiyapyco-master/yaml_files.txt
   ```  

*** Note *** : Please change the target output file to ....../merge_configs/hiyapyco-master/yaml_files.txt

3. Go to merge_configs/hiyapyco-master/

run ``` python get_yamls.py ```

3. Two files full_yaml.yaml, merged_yaml.yaml will be generated
   
      full_yaml.yaml simply grasps all the content from all .yaml files

      merged_yaml gives the minimum items from all .yaml files with overwriting the overlay items

