import os 
import yaml
import yaml.loader
import yaml.dumper
import yaml.representer
from collections import OrderedDict
import hiyapyco



ODYLDoYAMLMAPS = [
    'tag:yaml.org,2002:map',
    'tag:yaml.org,2002:omap',
    ]

class ODYL(yaml.SafeLoader):
    """Ordered Dict Yaml Loader"""
    def __init__(self, *args, **kwargs):
        yaml.SafeLoader.__init__(self, *args, **kwargs)
        for mapping in ODYLDoYAMLMAPS:
            self.add_constructor(mapping, type(self)._odyload)

    # see pyyaml constructors construct_*
    def _odyload(self, node):
        if node.tag not in ODYLDoYAMLMAPS:
            raise Exception('called ODYLoad for unregistered mapping mode "%s" aka. "%s"' % (node.tag, node.id,))
        data = OrderedDict()
        yield data
        data.update(self.construct_mapping(node))

    # see pyyaml construct_mapping
    def construct_mapping(self, node, deep=False):
        self.flatten_mapping(node)
        m = OrderedDict()
        for k, v in node.value:
            m[self.construct_object(k, deep=deep)] = self.construct_object(v, deep=deep)
        return m


class ODYD(yaml.SafeDumper):
    """Ordered Dict Yaml Dumper"""
    def __init__(self, *args, **kwargs):
        yaml.SafeDumper.__init__(self, *args, **kwargs)
        yaml.representer.SafeRepresenter.add_representer(OrderedDict, type(self)._odyrepr)
    def _odyrepr(self, data):
        """see: yaml.representer.represent_mapping"""
        return self.represent_mapping('tag:yaml.org,2002:map', data.items())



def load_data(stream):
    return yaml.load_all(stream, Loader=yaml.SafeLoader)

def full_content(source):
    full_content_dict = {}
    for yamlfile in source:
        fn = yamlfile 
        if not os.path.isabs(yamlfile):
            fn = os.path.join(os.getcwd(), fn)
        with open(fn, 'r') as f:
            content = load_data(f)
            full_content_dict[yamlfile] = list(content)[0]
    full_content = yaml.dump(full_content_dict)
    return full_content
        
if __name__ == '__main__':

    source = []
    with open('hiyapyco-master/yaml_files.txt', 'r') as f:
        content = f.readlines()
    for path in content:
        source.append(path.strip())

    print("Full content")
    full_output = full_content(source)
    with open('full_yaml.yaml', 'w') as outfile:
        outfile.write(full_output)
    print(full_output)

    print("Merged content")
    conf = hiyapyco.load(source, method=hiyapyco.METHOD_SUBSTITUTE)
    merged_output = hiyapyco.dump(conf, default_flow_style=False)
    with open('merged_yaml.yaml', 'w') as outfile:
        outfile.write(merged_output)
    print(merged_output)